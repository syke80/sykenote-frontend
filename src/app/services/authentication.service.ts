import { Injectable, EventEmitter } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ConfigService } from './config.service';
import { LoginResponseModel } from '../models/loginResponse.model';
import { LoginRequestModel } from '../models/loginRequest.model';
import { RegistrationRequestModel } from '../models/registrationRequest.model';
import { UserDetailsModel } from '../models/userDetails.model';

@Injectable()

export class AuthenticationService {
    public loggedIn$: EventEmitter<string>;
    private authenticationUrlPath: string = '/login';
    private registrationUrlPath: string = '/user';
    private token: string;

    constructor(private http: Http, private configService: ConfigService) {
        this.loggedIn$ = new EventEmitter();
    }

    private getTokenFromServer(email: string, password: string): Promise<LoginResponseModel> {
        let parameters: LoginRequestModel = {
            email: email,
            password: password
        };

        return this.http.post(this.configService.getOption('apiUrl') + this.authenticationUrlPath, parameters)
            .toPromise()
            .then( function(response) {
                return response.json();
            })
            .catch(this.handleHttpError);
    }

    private getUserInfoFromApi(): Promise<UserDetailsModel> {
        let options: RequestOptions = new RequestOptions({ headers: this.getHeaders() });

        return this.http.get(this.configService.getOption('apiUrl') + this.authenticationUrlPath, options)
            .toPromise()
            .then( (response: Response) => response.json() )
            .catch(this.handleHttpError);
    }

    private deleteToken(): void {
        delete this.token;
        localStorage.removeItem('token');
    }

    getHeaders(): Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer:' + this.getToken()
        });
    }

    getToken(): string {
        return this.token || localStorage.getItem('token');
    }

    logout(): void {
        this.deleteToken();
    }

    getUserInfo(): Promise<UserDetailsModel> {
        return this.getUserInfoFromApi();
    }

    login(email: string, password: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.getTokenFromServer(email, password).then((response) => {
                if (response) {
                    this.token = response.token;
                    localStorage.setItem('token', response.token);
                    this.loggedIn$.emit(response.token);
                    resolve(true);
                } else {
                    // TODO: redirect to main page
                    reject(false);
                }
            });
        });
    }

    register(email: string, password: string): Promise<boolean> {
        let parameters: RegistrationRequestModel = {
            email: email,
            password: password
        };

        return this.http.post(this.configService.getOption('apiUrl') + this.registrationUrlPath, parameters)
            .toPromise()
            .then( function(response) {
                return response.json();
            })
            .catch(this.handleHttpError);
    }

    handleHttpError(error): boolean {
        console.log('handle http error @ authentication service', error);
        if (error.status === 422) {
            return false;
        }
        console.log('Some error occured while communicating with endpoint.', error);
    }

}
